using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using TORO.Infra.Data.Context;
using TORO.Infrastructure.IoC;
using TORO.Application.Middlewares;
using TORO.Infra.Migrations;
using TORO.Infrastructure.IoC.Provider;
using TORO.Application.App;

namespace TORO.Test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        private static void RegisterServices(IServiceCollection services, IConfigurationSection appSettings)
        {
            DependencyContainer.RegisterServices(services);
            DependencyContainer.DocumentSwagger(services);
            DependencyContainer.JwtService(services, appSettings);
            DependencyContainer.AutoMapper(services);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            RegisterServices(services, appSettingsSection);

            services.AddHttpContextAccessor();
            services.AddCors();
            services.AddControllers()
                .AddJsonOptions(x => x.JsonSerializerOptions.IgnoreNullValues = true);

            services.AddDbContext<ToroContext>(options =>
            {
                options.UseSqlServer(
                    Configuration.GetConnectionString("localhost"));
                //,                     b => b.MigrationsAssembly("TORO.WebApi"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ServiceLocator.Initialize(app.ApplicationServices.GetService<IServiceProviderProxy>());
            AppHttpContext.Services = app.ApplicationServices;

            if (env.IsDevelopment())
            {                
                UserSeed.Initialize(AppHttpContext.Services);
                UserSeed.SeedData(AppHttpContext.Services);

                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "TORO INTEGRATION V1"));

            app.UseRouting();
            app.UseCustomExceptionMiddleware();
            app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
