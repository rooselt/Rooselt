﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TORO.Application.Interfaces;
using TORO.Application.Services;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/trends")]
    public class TrendsController : ControllerBase
    {
        private readonly ITrendsService _trendsService;
        private readonly IOrderService _orderService;
        private readonly IPositionService _positionService;
        private IMapper _mapper;
        public TrendsController(
            ITrendsService trendsService,
            IOrderService orderService,
            IPositionService positionService,
            IMapper mapper)
        {
            _mapper = mapper;
            _orderService = orderService;
            _trendsService = trendsService;
            _positionService = positionService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegisterTrendsViewModel register)
        {
            var trends = _mapper.Map<RegisterTrendsViewModel, Trends>(register);
            var model = await _trendsService.InsertOrUpdate(trends);
            return Ok(model.Trend);
        }

        [HttpPost("buy")]
        public async Task<IActionResult> Post(int id, int idUser, int amount)
        {
            var trend = await _trendsService.GetTrend(id);
            var order = await _orderService.InsertOrUpdate(trend, idUser, amount);

            if (order.Code == HttpStatusCode.OK)
            {
                await _positionService.InsertBuy(order.Order, trend);
                order.Order = null;
            }

            return Ok(order);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var idTrends = await _orderService.GetTrendsOrderSevenDays();
            var model = await _trendsService.GetTrends(idTrends);
            return Ok(model.Trends);
        }
    }
}
