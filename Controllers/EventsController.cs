﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TORO.Application.Interfaces;
using TORO.Application.Services;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/events")]
    public class EventsController : ControllerBase
    {
        private readonly IEventsService _eventsService;
        private readonly IUserPositionService _userPositionService;
        private IMapper _mapper;
        public EventsController(
            IUserPositionService userPositionService,
            IEventsService eventsService,            
            IMapper mapper)
        {
            _mapper = mapper;
            _userPositionService = userPositionService;
            _eventsService = eventsService;            
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegisterEventsViewModel register)
        {
            var events = _mapper.Map<RegisterEventsViewModel, Events>(register);
            var model = await _eventsService.Create(events);
            
            _userPositionService.UpdateAccount(model.Event);
  
            return Ok(model.Events);
        }
            
        [HttpGet]
        public async Task<IActionResult> Get(string cpf, int perPage, int page)
        {
         
            var model = await _eventsService.GetEvents(cpf, perPage, page);            
            return Ok(model.Events);
        }
    }
}
