﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TORO.Application.Interfaces;
using TORO.Application.Model;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.Test.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/user-position")]
    public class UserPositionController : ControllerBase
    {
        private readonly IUserPositionService _userPositionService;
        private IMapper _mapper;
        public UserPositionController(IUserPositionService userPositionService, IMapper mapper)
        {
            _mapper = mapper;
            _userPositionService = userPositionService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegisterUserPositionViewModel register)
        {
            var userPosition = _mapper.Map<RegisterUserPositionViewModel, UserPosition>(register);
            var model = await _userPositionService.InsertOrUpdate(userPosition);
            return base.Ok((object)model.UserPosition);
        }

        [HttpGet("{idUser}/{symbol}")]
        public async Task<IActionResult> Get(int idUser, string symbol)
        {
            var model = await _userPositionService.GetUserPosition(idUser, symbol);
            return base.Ok((object)model.UserPosition);
        }

        [HttpGet("{idUser}")]
        public async Task<IActionResult> Get(int idUser)
        {
            var model = await _userPositionService.GetUserPosition(idUser);
            return base.Ok(model.UserPosition);
        }

    }
}
