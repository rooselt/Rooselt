﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TORO.Application.Interfaces;
using TORO.Application.Model;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.Test.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserPositionService _userPositionService;
        private IMapper _mapper;
        public UsersController(IUserService userService, IUserPositionService userPositionSrvice, IMapper mapper)
        {
            _mapper = mapper;
            _userService = userService;
            _userPositionService = userPositionSrvice;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model, Response);
            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            _userService.SetTokenCookie(response.RefreshToken, Response);

            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("refresh-token")]
        public IActionResult RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var response = _userService.RefreshToken(refreshToken, Response);
            if (response == null)
                return Unauthorized(new { message = "Invalid token" });

            _userService.SetTokenCookie(response.RefreshToken, Response);

            return Ok(response);
        }

        [HttpPost("revoke-token")]
        public IActionResult RevokeToken([FromBody] RevokeTokenRequest model)
        {
            // accept token from request body or cookie
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return BadRequest(new { message = "Token is required" });

            var response = _userService.RevokeToken(token, Response);

            if (!response)
                return NotFound(new { message = "Token not found" });

            return Ok(new { message = "Token revoked" });
        }

        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var model = await _userService.GetUser(id);
            return Ok(model.User);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterViewModel register)
        {
            var user = _mapper.Map<RegisterViewModel, User>(register);
            var model = await _userService.InsertOrUpdate(user);

            _userPositionService.Create(model.User.Id);

            return Ok(model.User);
        }
    }
}
