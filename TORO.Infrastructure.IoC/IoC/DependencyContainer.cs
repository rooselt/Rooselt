﻿using TORO.Application.Interfaces;
using TORO.Application.Services;
using Microsoft.Extensions.DependencyInjection;
using TORO.Infra.Data.Interface;
using TORO.Infra.Data.Repositories;
using System.Text;
using TORO.Application.Commom.Helper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using AutoMapper;
using TORO.Application.AutoMapper;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interfaces;
using TORO.Infrastructure.IoC.Provider;
using Microsoft.AspNetCore.Http;

namespace TORO.Infrastructure.IoC
{
    public class DependencyContainer
    {
       
        public static void RegisterServices(IServiceCollection services)
        {
            //Application
            services.AddScoped<IToroContext>(provider => provider.GetService<ToroContext>());
            services.AddScoped<ITrendsService, TrendsService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPositionService, PositionService>();
            services.AddScoped<IUserPositionService, UserPositionService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEventsService, EventsService>();

            //Infra
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IEventsRepository, EventsRepository>();
            services.AddScoped<ITrendsRepository, TrendsRepository>();
            services.AddScoped<IPositionsRepository, PositionRepository>();
            services.AddScoped<IUserPositionRepository, UserPositionRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();      
            services.AddSingleton<IServiceProviderProxy, HttpContextServiceProviderProxy>();
       
        }
        public static void AutoMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AllowNullCollections = true;
                mc.AllowNullDestinationValues = true;
                mc.AddProfile(new DomainToViewModelMappingProfile());
                mc.AddProfile(new ViewModelToDomainMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
        public static void JwtService(IServiceCollection services, IConfigurationSection appSettingsSection)
        {
            // configure strongly typed settings objects
          
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                };
            });          
        }
        public static void DocumentSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API TORO.INTEGRATION",
                    Version = "v1",
                    Description = "API DE INTEGRAÇÃO DA TORO",
                });
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                          {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                          },
                          Scheme = "oauth2",
                          Name = "Bearer",
                          In = ParameterLocation.Header,

                        },
                        new List<string>()
                      }
                    });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
        }
    }
}
