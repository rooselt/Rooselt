﻿using AutoMapper;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMapping";
              
        public ViewModelToDomainMappingProfile()
        {
            this.CreateMap<User, RegisterViewModel>();            
      
        }
    }
}