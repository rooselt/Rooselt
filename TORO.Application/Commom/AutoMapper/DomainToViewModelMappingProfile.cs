﻿using AutoMapper;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data;

namespace TORO.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelMapping";

        public DomainToViewModelMappingProfile()
        {
            this.CreateMap<RegisterViewModel, User>();
            this.CreateMap<RegisterUserPositionViewModel, UserPosition>();
            this.CreateMap<RegisterUserPositionViewModel, UserPositionDto>();
            this.CreateMap<RegisterTrendsViewModel, Trends>();
            this.CreateMap<RegisterEventsViewModel, Events>();
        }
    }
}