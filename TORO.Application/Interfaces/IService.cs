﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using TORO.Application.Model;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;

namespace TORO.Application.Interfaces
{
    public interface IUserPositionService
    {
        void Create(int idUser);
        Task<UserPositionViewModel> InsertOrUpdate(UserPosition userPosition);
        Task<UserPositionDtoViewModel> GetUserPosition(int idUser, string symbol);
        Task<UserPositionDtoViewModel> GetUserPosition(int idUser);
        void UpdateAccount(Events events);
    }
    public interface ITrendsService
    {
        Task<Trends> GetTrend(int idTrend);
        Task<TrendsViewModel> GetTrends(IEnumerable<int> idTrends);
        Task<TrendsViewModel> InsertOrUpdate(Trends trends);
    }

    public interface IOrderService
    {
        Task<OrderViewModel> InsertOrUpdate(Trends trends, int idUser, int amount);
        Task<IEnumerable<int>> GetTrendsOrderSevenDays();
    }

    public interface IPositionService
    {
        Task<PositionsViewModel> InsertOrUpdate(Positions positions);
        Task<PositionsViewModel> GetPositions(int idUserPosition);
        Task<PositionsViewModel> InsertBuy(Order order, Trends trend);
    }

    public interface IEventsService
    {
        Task<EventsViewModel> Create(Events events);
        Task<EventsViewModel> GetEvents(string cpf, int perpage, int page);
    }

    public interface IUserService
    {
        Task<UserViewModel> InsertOrUpdate(User User);
        Task<UserViewModel> GetUser(int idUser);
        bool RevokeToken(string token, HttpResponse response);
        void SetTokenCookie(string token, HttpResponse response);
        AuthenticateResponse RefreshToken(string token, HttpResponse response);
        AuthenticateResponse Authenticate(AuthenticateRequest model, HttpResponse response);
    }
}
