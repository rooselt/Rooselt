﻿using TORO.Application.Commom.Enum;

namespace TORO.Application.Interfaces
{
    public interface ICrudMsgFormater
    {
        string CreateSuccessCrudMessage();
        string CreateErrorCrudMessage();        
    }
}
