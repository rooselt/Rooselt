﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Threading.Tasks;
using TORO.Application.Interfaces;
using TORO.Application.Specifications;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;
using TORO.Infra.Data.Interfaces;

namespace TORO.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IToroContext _context;

        public OrderService(IOrderRepository orderRepository, IToroContext context)
        {
            _context = context;
            _orderRepository = orderRepository;
        }

        public async Task<IEnumerable<int>> GetTrendsOrderSevenDays()
        {
            var trendsOrderSevenDays = new TrendsSevenDayPolicy();
            var idTrends = await _orderRepository
                .ListTrends(trendsOrderSevenDays.Criteria);
            return idTrends;
        }

        public async Task<OrderViewModel> InsertOrUpdate(Trends trends, int idUser, int amount)
        {
            var userPosition = _context.UserPositions.
                Include(c => c.Positions)
               .FirstOrDefault(c => c.IdUser == idUser);

            if (userPosition == null)
                throw new ArgumentNullException("É necessario cadastrar a conta do Usuário");

            var model = new OrderViewModel();
            var total = trends.CurrentPrice * amount;            
            var valid = userPosition.ValidBalanceAmout(total);
            if (valid)
            {
                userPosition.CheckingAccountAmount = userPosition.CheckingAccountAmount - total;

                _context.UserPositions.Update(userPosition);
                _context.SaveChanges();

                model.Code = HttpStatusCode.OK;
                model.Order = await _orderRepository.Insert(trends, userPosition.Id);
                model.Message = "Compra do Ativo, realizada com Sucesso";
            }
            else
            {
                model.Code = HttpStatusCode.NotAcceptable;
                model.Message = "Cliente não possui saldo para realizar a compra do Ativo";
            }


            return model;
        }
    }
}
