﻿using System.Linq;
using System.Threading.Tasks;
using TORO.Application.Interfaces;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data;
using TORO.Infra.Data.Interface;

namespace TORO.Application.Services
{
    public class UserPositionService : IUserPositionService
    {
        public IUserPositionRepository _userPositionRepository;
        public UserPositionService(IUserPositionRepository userPositionRepository)
        {
            _userPositionRepository = userPositionRepository;
        }

        public async Task<UserPositionViewModel> InsertOrUpdate(UserPosition userPosition)
        {
            return new UserPositionViewModel()
            {
                UserPosition = await _userPositionRepository.InsertOrUpdate(userPosition)
            };
        }

        public void Create(int idUser)
        {
            var userPosition = new UserPosition()
            {
                IdUser = idUser
            };

            _userPositionRepository.Add(userPosition);
            _userPositionRepository.Save();
        }

        public void UpdateAccount(Events events)
        {
            var userPositon = _userPositionRepository
                .GetAll(c => c.User.Cpf == events.Cpf)
                .FirstOrDefault();

            userPositon.CheckingAccountAmount = 
                userPositon.CheckingAccountAmount + events.Amount;

            _userPositionRepository.Update(userPositon);
            _userPositionRepository.Save();
        }
        public async Task<UserPositionDtoViewModel> GetUserPosition(int idUser)
        {
            return new UserPositionDtoViewModel()
            {
                UserPosition = await _userPositionRepository.GetUserPosition(idUser)
            };
        }

        public async Task<UserPositionDtoViewModel> GetUserPosition(int idUser, string symbol)
        {
            return new UserPositionDtoViewModel()
            {                
                UserPosition = await _userPositionRepository.GetUserPosition(idUser, symbol)
            };
        }
    }
}
