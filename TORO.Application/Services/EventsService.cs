﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TORO.Application.Interfaces;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;

namespace TORO.Application.Services
{
    public class EventsService : IEventsService
    {
        public IEventsRepository _eventsRepository;

        public EventsService(IEventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }
        public async Task<EventsViewModel> Create(Events events)
        {
            return new EventsViewModel()
            {
                Event = await _eventsRepository.Create(events)
            };
        }
        public async Task<Events> GetEvent(int idEvent)
        {
            return await _eventsRepository.GetSingleAsync(c => c.Id == idEvent);
        }
        public async Task<EventsViewModel> GetEvents(string cpf, int perpage, int page)
        {
            return new EventsViewModel()
            {
                Statement = await _eventsRepository.GetEvents(cpf, perpage, page)
            };
        }
    }
}
