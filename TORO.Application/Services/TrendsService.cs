﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TORO.Application.Interfaces;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;

namespace TORO.Application.Services
{
    public class TrendsService : ITrendsService
    {
        public ITrendsRepository _trendsRepository;

        public TrendsService(ITrendsRepository trendsRepository)
        {
            _trendsRepository = trendsRepository;
        }
        public async Task<TrendsViewModel> InsertOrUpdate(Trends trends)
        {
            return new TrendsViewModel()
            {
                Trend = await _trendsRepository.InsertOrUpdate(trends)
            };
        }
        public async Task<Trends> GetTrend(int idTrend)
        {
            return await _trendsRepository.GetSingleAsync(c => c.Id == idTrend);
        }
        public async Task<TrendsViewModel> GetTrends(IEnumerable<int> idTrends)
        {
            return new TrendsViewModel()
            {
                Trends = await _trendsRepository.GetTrends(idTrends)
            };
        }
    }
}
