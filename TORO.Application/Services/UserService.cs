﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TORO.Application.Commom.Helper;
using TORO.Application.Interfaces;
using TORO.Application.Model;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;


namespace TORO.Application.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        public IUserRepository _userRepository;
        public UserService(IUserRepository userRepository, IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
            _userRepository = userRepository;
        }

        public async Task<UserViewModel> InsertOrUpdate(User user)
        {
            return new UserViewModel()
            {
                User = await _userRepository.InsertOrUpdate(user)
            };
        }

        public async Task<UserViewModel> GetUser(int idUser)
        {
            return new UserViewModel()
            {
                User = await _userRepository.GetUser(idUser)
            };
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model, HttpResponse response)
        {
            var user = _userRepository.GetAll()
                .Include(x => x.RefreshTokens)
                .FirstOrDefault(x => x.Username == model.Username && x.Password == model.Password);

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt and refresh tokens
            var ipAddress = IpAddress(response);
            var jwtToken = GenerateJwtToken(user);
            var refreshToken = GenerateRefreshToken(ipAddress);

            // save refresh token
            user.RefreshTokens.Add(refreshToken);
                        
            user.Token = jwtToken;

            _userRepository.Update(user);
            _userRepository.Save();

            return new AuthenticateResponse(user, jwtToken, refreshToken.Token);
        }
        public AuthenticateResponse RefreshToken(string token, HttpResponse response)
        {
            var user = _userRepository.GetAll()
                .SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            // return null if no user found with token
            if (user == null) return null;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // return null if token is no longer active
            if (!refreshToken.IsActive) return null;

            // replace old refresh token with a new one and save
            var ipAddress = IpAddress(response);
            var newRefreshToken = GenerateRefreshToken(ipAddress);

            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;

            user.RefreshTokens.Add(newRefreshToken);

            _userRepository.Update(user);
            _userRepository.Save();

            // generate new jwt
            var jwtToken = GenerateJwtToken(user);

            return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token);
        }
        public bool RevokeToken(string token, HttpResponse response)
        {
            var user = _userRepository.GetAll().
                SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            // return false if no user found with token
            if (user == null) return false;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // return false if token is not active
            if (!refreshToken.IsActive) return false;

            // revoke token and save
            var ipAddress = IpAddress(response);

            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;

            _userRepository.Update(user);
            _userRepository.Save();

            return true;
        }
        public void SetTokenCookie(string token, HttpResponse response)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            response.Cookies.Append("refreshToken", token, cookieOptions);
        }
        private string GenerateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, "user")
                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        private RefreshToken GenerateRefreshToken(string ipAddress)
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);

            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddDays(7),
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };
        }
        private string IpAddress(HttpResponse response)
        {
            return response.Headers.ContainsKey("X-Forwarded-For")
                ? (string)response.Headers["X-Forwarded-For"]
                : response.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

    }
}
