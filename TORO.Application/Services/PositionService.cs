﻿using System.Threading.Tasks;
using TORO.Application.Interfaces;
using TORO.Application.ViewModels;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;

namespace TORO.Application.Services
{
    public class PositionService : IPositionService
    {
        public IPositionsRepository _positionsRepository;
        public PositionService(IPositionsRepository positionsRepository)
        {
            _positionsRepository = positionsRepository;
        }
        public async Task<PositionsViewModel> GetPositions(int idUserPosition)
        {
            return new PositionsViewModel()
            {
                Positions = await _positionsRepository.GetPositions(idUserPosition)
            };
        }

        public async Task<PositionsViewModel> InsertBuy(Order order, Trends trend)
        {
            return new PositionsViewModel
            {
                Position = await _positionsRepository.InsertBuy(order, trend)
            };
        }

        public async Task<PositionsViewModel> InsertOrUpdate(Positions positions)
        {
            return new PositionsViewModel()
            {
                Position = await _positionsRepository.InsertOrUpdate(positions)
            };
        }
    }
}
