﻿using System.Collections.Generic;
using TORO.Domain.Entities;

namespace TORO.Application.ViewModels
{
    public class EventsViewModel
    {
        public object Statement { get; set; }
        public Events Event { get; set; }
        public IEnumerable<Events> Events { get; set; }
    }

    public class RegisterEventsViewModel
    {        
        public string Cpf { get; set; }
        public string Event { get; set; }
        public Target Target { get; set; }
        public Origin Origin { get; set; }
        public int Amount { get; set; }
    }
}
