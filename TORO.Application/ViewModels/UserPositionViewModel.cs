﻿using System.Collections.Generic;
using TORO.Domain.Entities;
using TORO.Infra.Data;

namespace TORO.Application.ViewModels
{
    public class UserPositionDtoViewModel
    {   public UserPositionDto UserPosition { get; set; }
    }

    public class UserPositionViewModel
    {
        public UserPosition UserPosition { get; set; }
    }

    public class RegisterUserPositionViewModel
    {
        public int IdUser { get; set; }
        public decimal CheckingAccountAmount { get; set; }
        public decimal Consolidated { get; set; }        
        public ICollection<Positions> Positions { get; set; }
    }
}
