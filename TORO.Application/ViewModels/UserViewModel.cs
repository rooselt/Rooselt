﻿using TORO.Domain.Entities;

namespace TORO.Application.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }
    }

    public class RegisterViewModel
    {   
        public string Cpf { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
