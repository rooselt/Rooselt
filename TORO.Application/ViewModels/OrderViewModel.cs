﻿using System.Collections.Generic;
using System.Net;
using TORO.Domain.Entities;

namespace TORO.Application.ViewModels
{
    public class OrderViewModel
    {
        public Order Order { get; set; }
        public HttpStatusCode Code { get;  set; }
        public string Message { get;  set; }
    }
}
