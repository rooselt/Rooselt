﻿using System.Collections.Generic;
using TORO.Domain.Entities;

namespace TORO.Application.ViewModels
{
    public class TrendsViewModel
    {
        public Trends Trend { get; set; }
        public IEnumerable<Trends> Trends { get; set; }
    }


    public class RegisterTrendsViewModel
    {        
        public string Symbol { get; set; }
        public decimal CurrentPrice { get; set; }        
    }
}
