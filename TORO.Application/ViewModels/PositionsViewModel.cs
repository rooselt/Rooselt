﻿using System.Collections.Generic;
using TORO.Domain.Entities;

namespace TORO.Application.ViewModels
{
    public class PositionsViewModel
    {
        public Positions Position { get; set; }
        public IEnumerable<Positions> Positions { get; set; }
    }
}
