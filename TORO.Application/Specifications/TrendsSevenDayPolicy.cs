﻿using System;
using System.Linq.Expressions;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interface;


namespace TORO.Application.Specifications
{
    public class TrendsSevenDayPolicy : ISpecification<Order>
    {
        public TrendsSevenDayPolicy()
        {
            var anyLessSevenDay = DateTime.Now.AddDays(-7).Date;

            Criteria =
                    c => c.Created.Value.Date >= anyLessSevenDay &&
                           c.Created.Value.Date <= DateTime.Now.Date;
        }
        public Expression<Func<Order, bool>> Criteria { get; }
    }
}
