﻿namespace TORO.Application.Model
{
    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }
}
