﻿using System.ComponentModel.DataAnnotations;

namespace TORO.Application.Model
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
