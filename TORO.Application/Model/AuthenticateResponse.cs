﻿using System.Text.Json.Serialization;
using TORO.Domain.Entities;

namespace TORO.Application.Model
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Cpf { get; set; }
        public string Token { get; set; }

        [JsonIgnore]
        public string RefreshToken { get; set; }

        public AuthenticateResponse(User user,
            string jwtToken,
            string refreshToken)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Username = user.Username;
            Cpf = user.Cpf;
            Token = jwtToken;
            RefreshToken = refreshToken;
        }
    }
}
