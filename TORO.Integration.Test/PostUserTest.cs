﻿using System.Threading.Tasks;
using TORO.Domain.Entities;
using TORO.Infra.Data.Repositories;
using Xunit;

namespace TORO.Integration.Test
{
    public class PostUserTest : BaseTest
    {        
        #region FACT
        [Fact]
        public void Fact_PostUser_NoClassNoRepository()
        {
            // EXAMPLE
            var user = new User("JOAO", "CUNHA", "joao", "123456");

            // REPOSITORY
            ctx.User.Add(user);
            ctx.SaveChanges();

            // ASSERT 
            Assert.Equal(1, user.Id);
        }

        [Fact]
        public void Fact_PostUser_NoRepository()
        {
            // EXAMPLE
            var user = new User("JOAO", "CUNHA", "joao", "123456")
            {
                Id = 0
            };

            // REPOSITORY
            ctx.User.Add(user);
            ctx.SaveChanges();

            // ASSERT 
            Assert.Equal(1, user.Id);
        }

        [Fact]
        public async Task Fact_PostUser_NoValidation()
        {
            // EXAMPLE
            var user = new User("JOAO", "CUNHA", "joao", "123456")
            {
                Id = 0
            };

            // REPOSITORY
            user = await new UserRepository(ctx).InsertOrUpdate(user);

            // ASSERT 
            Assert.Equal(1, user.Id);
        }        
        #endregion
    }
}
