﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TORO.Domain.Entities;
using TORO.Infra.Data.Repositories;
using Xunit;

namespace TORO.Integration.Test
{
  public  class PostUserPositionPositionTest : BaseTest
    {
        private const decimal Result = 800;
        private UserPosition TestEntries()
        {
            var userPosition = new UserPosition
            {
                CheckingAccountAmount = 200,
                Created = DateTime.Now,
                IdUser = 1
            };

            return userPosition;
        }

        #region FACT
        [Fact]
        public void Fact_PostUserPosition_NoClassNoRepository()
        {
            // EXAMPLE
            var userPosition = TestEntries();

            // REPOSITORY
            ctx.UserPositions.Add(userPosition);
            ctx.SaveChanges();

            // ASSERT 
            Assert.Equal(1, userPosition.Id);
        }

        [Fact]
        public void Fact_PostUserPosition_NoRepository()
        {
            var userPosition = TestEntries();

            // REPOSITORY
            ctx.UserPositions.Add(userPosition);
            ctx.SaveChanges();

            // ASSERT 
            Assert.Equal(1, userPosition.Id);
        }

        [Fact]
        public async Task Fact_PostUserPosition_NoValidation()
        {
            // EXAMPLE
            var userPosition = TestEntries();

            // REPOSITORY
            userPosition = await new UserPositionRepository(ctx)
                .InsertOrUpdate(userPosition);

            // ASSERT 
            Assert.Equal(1, userPosition.Id);
        }


        [Fact]
        public async Task Fact_Post_Consolidaded_UserPosition_Validation()
        {
            // EXAMPLE
            var userPosition = TestEntries();

            //INCLUDE IN PURCHASE 
            //(Amount = 300 * CurrentPrice = 2) + CheckingAccountAmount = 200
            //Result = 800
            userPosition.Positions = new List<Positions>() {
                new Positions { 
                    Amount  = 300 , 
                    CurrentPrice = 2, 
                    Symbol = "PETR2"  }
            };

            // REPOSITORY
            userPosition = await new UserPositionRepository(ctx)
                .InsertOrUpdate(userPosition);

            // ASSERT 
            Assert.Equal(Result, userPosition.Consolidated);
        }
        #endregion
    }
}


