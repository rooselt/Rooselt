﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interfaces;

namespace TORO.Infra.Data.Context
{
    public class ToroContext : DbContext, IToroContext
    {      
        public ToroContext(DbContextOptions<ToroContext> options) : base(options)
        {
            
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Positions> Positions { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserPosition> UserPositions { get; set; }
        public virtual DbSet<Target> Target { get; set; }
        public virtual DbSet<Origin> Origin { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var assembly = Assembly.GetExecutingAssembly();

            modelBuilder.ApplyConfigurationsFromAssembly(assembly, c => c.FullName.Contains("Mapping"));

        }       
    }
}
