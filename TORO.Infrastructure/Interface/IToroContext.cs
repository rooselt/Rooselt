﻿using Microsoft.EntityFrameworkCore;
using TORO.Domain.Entities;

namespace TORO.Infra.Data.Interfaces
{
    public interface IToroContext
    {
        DbSet<Positions> Positions { get; set; }
        DbSet<User> User { get; set; }
        DbSet<UserPosition> UserPositions { get; set; }
        int SaveChanges();
    }
}
