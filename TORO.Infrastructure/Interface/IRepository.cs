﻿using OfferingSolutions.GenericEFCore.RepositoryContext;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TORO.Domain.Entities;
using TORO.Infra.Data.Interfaces;

namespace TORO.Infra.Data.Interface
{
    public interface IEventsRepository : IGenericRepositoryContext<Events>
    {
        Task<Events> Create(Events events);
        Task<object> GetEvents(string cpf, int perPage, int page);
    }
    public interface IOriginRepository : IGenericRepositoryContext<Origin>
    {
        Task<IEnumerable<Origin>> GetOrigin();
    }
    public interface ITrendsRepository : IGenericRepositoryContext<Trends>
    {
        Task<IEnumerable<Trends>> GetTrends(IEnumerable<int> idTrends);
        Task<Trends> InsertOrUpdate(Trends trends);
    }

    public interface ITargetRepository : IGenericRepositoryContext<Target>
    {
        Task<Target> GetTarget(int idEvents);
    }
    public interface IOrderRepository : IGenericRepositoryContext<Order>
    {
        Task<Order> Insert(Trends trends, int idUserPosition);
        Task<IEnumerable<int>> ListTrends(Expression<Func<Order, bool>> expression);
    }
    public interface IPositionsRepository : IGenericRepositoryContext<Positions>
    {
        Task<Positions> InsertBuy(Order order, Trends trends);
        Task<Positions> InsertOrUpdate(Positions position);
        Task<IEnumerable<Positions>> GetPositions(int idUserPosition);
    }
    public interface IUserPositionRepository : IGenericRepositoryContext<UserPosition>
    {        
        Task<UserPosition> InsertOrUpdate(UserPosition userPosition);
        Task<UserPositionDto> GetUserPosition(int idUser, string symbol);
        Task<UserPositionDto> GetUserPosition(int idUser);
    }
    public interface IUserRepository : IGenericRepositoryContext<User>
    {
        Task<User> InsertOrUpdate(User user);
        Task<User> GetUser(int id);
    }
}
