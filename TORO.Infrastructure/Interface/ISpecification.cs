﻿using System;
using System.Linq.Expressions;

namespace TORO.Infra.Data.Interface
{
    public interface ISpecification<T>
    {
        Expression<Func<T, bool>> Criteria { get; }
    }
}
