using System.Collections.Generic;
using TORO.Domain.Entities;

namespace TORO.Infra.Data
{
    public class UserPositionDto
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public decimal CheckingAccountAmount { get; set; }
        public decimal Consolidated { get; set; }
        public int Quantity { get; set; }
        public virtual ICollection<Positions> Positions { get; set; }

    }
}