﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TORO.Infra.Data.Dto
{
    public class OrderDto
    {
        public string Symbol { get; set; }
        public decimal Amount { get; set; }
    }
}
