﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class TrendsRepository : GenericRepositoryContext<Trends>, ITrendsRepository
    {
        public ToroContext _context;
        public TrendsRepository(ToroContext context) : base(context)
        {
            _context = context;
        }

        
        public async Task<Trends> InsertOrUpdate(Trends trends)
        {
            var data = await GetAll()
                .Where(c => c.Id == trends.Id || c.Symbol == trends.Symbol)
                .FirstOrDefaultAsync();

            if (data == null)
            {
                Add(trends);
                Save();

                return data;
            }

            data.CurrentPrice = trends.CurrentPrice;
            data.Symbol = data.Symbol;

            Update(data);
            Save();

            return data;
        }
                
        public async Task<IEnumerable<Trends>> GetTrends(IEnumerable<int> idTrends)
        {
            var data = await GetAll()
                .Where(c => idTrends.Contains(c.Id) || idTrends.Any() == false)
                .ToListAsync();

            return data;
        }
    }
}
