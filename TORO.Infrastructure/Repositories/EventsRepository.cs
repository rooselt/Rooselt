﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class EventsRepository : GenericRepositoryContext<Events>, IEventsRepository
    {
        public ToroContext _context;
        public EventsRepository(ToroContext context) : base(context)
        {
            _context = context;
        }
        public async Task<Events> Create(Events events)       
        {  
            Add(events);
            await SaveASync();

            return events;
        }
        public async Task<object> GetEvents(string cpf, int perPage, int page)
        {
            var data = await GetAll()
                .Where(c => c.Cpf == cpf)
                .OrderByDescending(c => c.Created)
                .Skip((perPage - 1) * page)
                .Take(page)
                .ToListAsync();

            var total = data.Count;
            var totalPage = (decimal)total / page;
            return new
            {
                data = data,
                count = total,
                currentPage = page,
                totalPage = (int)Math.Ceiling(totalPage)
            };
        }
    }
}
