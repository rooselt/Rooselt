﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class PositionRepository : GenericRepositoryContext<Positions>, IPositionsRepository
    {
        public ToroContext _context;
        public PositionRepository(ToroContext context) : base(context)
        {
            _context = context;
        }
        public async Task<Positions> InsertOrUpdate(Positions position)
        {
            var data = await GetAll()
                .Where(c => c.Id == position.Id)
                .FirstOrDefaultAsync();

            if (data == null)
            {
                Add(position);
                return position;
            }

            data.CurrentPrice = position.CurrentPrice;
            data.Amount = position.Amount;
            data.Symbol = data.Symbol;

            Update(data);
            Save();

            return data;
        }

        public async Task<Positions> InsertBuy(Order order, Trends trends)
        {
            var position = new Positions
            {
                Amount = order.Amount,
                CurrentPrice = trends.CurrentPrice,
                IdUserPosition = order.IdUserPosition,
                Symbol = trends.Symbol,
                Created = DateTime.Now,
            };

            Add(position);
            await SaveASync();

            return position;
        }
        public async Task<IEnumerable<Positions>> GetPositions(int idUserPosition)
        {
            var data = await GetAll()
                .Where(c => c.IdUserPosition == idUserPosition)
                .ToListAsync();

            return data;
        }
    }
}
