﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class UserPositionRepository : GenericRepositoryContext<UserPosition>, IUserPositionRepository
    {
        public ToroContext _context;
        public UserPositionRepository(ToroContext context) : base(context)
        {
            _context = context;
        }

        public async Task<UserPosition> InsertOrUpdate(UserPosition userPosition)
        {
            var data = await GetAll()
                .Where(c => c.Id == userPosition.Id)
                .FirstOrDefaultAsync();

            if (data == null)
            {                
                Add(userPosition);
                Save();

                return userPosition;
            }

            data.CheckingAccountAmount = userPosition.CheckingAccountAmount;

            Update(data);
            Save();

            return data;
        }  

        public async Task<UserPositionDto> GetUserPosition(int idUser)
        {
            var data = await GetAll()
                .Where(c => c.IdUser == idUser)
                .Select(c => new UserPositionDto
                {
                    CheckingAccountAmount = c.CheckingAccountAmount,
                    Consolidated = c.Consolidated,
                    Quantity = c.Positions.Sum(c => c.Amount),
                    Positions = c.Positions.ToList()
                }).FirstOrDefaultAsync();

            return data;
        }

        public async Task<UserPositionDto> GetUserPosition(int idUser, string symbol)
        {
            var data = await GetAll()
                .Include(c => c.Positions)
                .Where(c => c.IdUser == idUser && c.Positions.Any(c => c.Symbol == symbol))
                .Select(c => new UserPositionDto
                {
                    CheckingAccountAmount = c.CheckingAccountAmount,
                    Consolidated = c.Consolidated,
                    Quantity = c.Positions.Sum(c=> c.Amount)
                }).FirstOrDefaultAsync();

            return data;
        }       
    }
}
