﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class OrderRepository : GenericRepositoryContext<Order>, IOrderRepository
    {
        public ToroContext _context;
        public OrderRepository(ToroContext context) : base(context)
        {
            _context = context;
        }
        public async Task<Order> Insert(Trends trends, int idUserPosition)
        {
            var data = await GetAll()
                .Where(c => c.IdUserPosition == idUserPosition)
                .ToListAsync();

            var amount = data.Count() + 1;
            var order = new Order
            {
                IdUserPosition = idUserPosition,
                IdTrend = trends.Id,
                Symbol = trends.Symbol,
                Amount = amount
            };

            Add(order);
            Save();

            return order;
        }
        public async Task<IEnumerable<int>> ListTrends(Expression<System.Func<Order, bool>> expression)
        { 
            var list = await GetAll(expression)                
                .Select(c => c.IdTrend).ToListAsync();

            var item = list
                 .GroupBy(c => c)
                .OrderByDescending(grp => grp.Count())
                .Take(5)
                .Select(c => c.First());

            return item;
        }       
    }
}
