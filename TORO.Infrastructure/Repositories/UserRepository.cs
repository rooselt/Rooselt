﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OfferingSolutions.GenericEFCore.RepositoryContext;
using TORO.Application.Commom.Enum;
using TORO.Domain.Entities;
using TORO.Infra.Data.Context;
using TORO.Infra.Data.Interface;

namespace TORO.Infra.Data.Repositories
{
    public class UserRepository : GenericRepositoryContext<User>, IUserRepository
    {
        public ToroContext _context;
        public UserRepository(ToroContext context) : base(context)
        {
            _context = context;
        }

        public async Task<User> InsertOrUpdate(User user)
        {
            var data = await GetAll()
                .Where(c => c.Id == user.Id)
                .FirstOrDefaultAsync();

            if (data == null)
            {
                var code = (int)BankCode.Code;
                user.Code = code.ToString();

                Add(user);
                Save();

                return user;
            }

            data.FirstName = user.FirstName;
            data.Cpf = user.Cpf;
            data.LastName = user.LastName;

            Update(data);
            Save();

            return data;
        }
        public async Task<User> GetUser(int id)
        {
            var data = await GetAll()
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();

            return data;
        }
    }
}
