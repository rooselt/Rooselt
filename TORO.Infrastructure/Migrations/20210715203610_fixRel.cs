﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TORO.Infra.Data.Migrations
{
    public partial class fixRel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Origin_Events_IdEvents",
                table: "Origin");

            migrationBuilder.DropForeignKey(
                name: "FK_Target_Events_IdEvents",
                table: "Target");

            migrationBuilder.DropIndex(
                name: "IX_Target_IdEvents",
                table: "Target");

            migrationBuilder.DropIndex(
                name: "IX_Origin_IdEvents",
                table: "Origin");

            migrationBuilder.DropColumn(
                name: "IdEvents",
                table: "Target");

            migrationBuilder.DropColumn(
                name: "IdEvents",
                table: "Origin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdEvents",
                table: "Target",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdEvents",
                table: "Origin",
                type: "int",
                maxLength: 20,
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Target_IdEvents",
                table: "Target",
                column: "IdEvents");

            migrationBuilder.CreateIndex(
                name: "IX_Origin_IdEvents",
                table: "Origin",
                column: "IdEvents");

            migrationBuilder.AddForeignKey(
                name: "FK_Origin_Events_IdEvents",
                table: "Origin",
                column: "IdEvents",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Target_Events_IdEvents",
                table: "Target",
                column: "IdEvents",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
