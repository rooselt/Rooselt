﻿namespace TORO.Infra.Migrations
{
    using TORO.Infra.Data.Context;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Threading.Tasks;
    using System.Linq;
    using TORO.Domain.Entities;

    public class UserSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using var serviceScope = serviceProvider.CreateScope();
            var services = serviceScope.ServiceProvider;
            var context = services.GetService<ToroContext>();

            if (context != null)
            {

                context.Database.EnsureCreated();
            }
        }

        public static void SeedData(IServiceProvider serviceProvider)
        {
            using var serviceScope = serviceProvider.CreateScope();
            var services = serviceScope.ServiceProvider;
            var context = services.GetService<ToroContext>();

            //add admin user
            if (!context.User.Any())
            {
                var adminUser = new User("Administrador", string.Empty, "admin", "admin1234");
                adminUser.Cpf = string.Empty;

                context.User.Add(adminUser);
            }
            context.SaveChanges();
        }
    }
}
    
