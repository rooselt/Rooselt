﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TORO.Infra.Data.Migrations
{
    public partial class addColumnUserPositionOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdUserPosition",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Order_IdUserPosition",
                table: "Order",
                column: "IdUserPosition");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_UserPositions_IdUserPosition",
                table: "Order",
                column: "IdUserPosition",
                principalTable: "UserPositions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_UserPositions_IdUserPosition",
                table: "Order");

            migrationBuilder.DropIndex(
                name: "IX_Order_IdUserPosition",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "IdUserPosition",
                table: "Order");
        }
    }
}
