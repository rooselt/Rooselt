﻿using TORO.Domain.Entities;
using TORO.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class UserMap : BaseMap<User>, IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            DefaultConfigs(builder);

            builder.Property(x => x.Username).IsRequired();
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x => x.Code).IsRequired(false);
            builder.Property(x => x.Cpf).HasMaxLength(11).IsRequired();
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.LastName).IsRequired(false);
            builder.Property(x => x.Token).IsRequired(false);

            builder.Property(x => x.IsDeleting).HasDefaultValue(false).IsRequired();            
        }
    }
}
