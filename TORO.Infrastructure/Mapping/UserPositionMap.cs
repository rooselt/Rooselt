﻿using TORO.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class UserPositionMap : BaseMap<UserPosition>, IEntityTypeConfiguration<UserPosition>
    {
        public void Configure(EntityTypeBuilder<UserPosition> builder)
        {
            DefaultConfigs(builder);

            builder.Property(x => x.IdUser).IsRequired();
            builder.Property(x => x.CheckingAccountAmount)
                .HasColumnType("decimal(18,2)").IsRequired();
            builder.Property(x => x.Consolidated)
                .HasColumnType("decimal(18,2)").IsRequired();

            builder.HasOne(x => x.User).WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.IdUser);                

        }
    }
}
