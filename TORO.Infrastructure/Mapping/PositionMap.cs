﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TORO.Domain.Entities;

namespace TORO.Infra.Data.Mapping
{
    public class PositionMap : BaseMap<Positions>, IEntityTypeConfiguration<Positions>
    {
        public void Configure(EntityTypeBuilder<Positions> builder)
        {
            DefaultConfigs(builder);

            builder.Property(x => x.IdUserPosition).IsRequired();
            builder.Property(x => x.CurrentPrice).HasColumnType("decimal(18,2)").IsRequired();
            builder.Property(x => x.Amount).IsRequired();
            builder.Property(x => x.Symbol).IsRequired();

            builder.HasOne(x => x.UserPosition).WithMany(c=> c.Positions)                
                .HasForeignKey(x => x.IdUserPosition)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
