﻿
using TORO.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class OrderMap : BaseMap<Order>, IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(x => x.IdTrend).IsRequired();
            builder.Property(x => x.IdUserPosition).IsRequired();
            builder.Property(x => x.Amount).IsRequired();   
            builder.Property(x => x.Symbol).IsRequired();

            builder.HasOne(x => x.Trends).WithMany(c=> c.Order)
                .HasForeignKey(x => x.IdTrend)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.UserPosition).WithMany()
               .HasForeignKey(x => x.IdUserPosition)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
