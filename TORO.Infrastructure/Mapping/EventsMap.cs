﻿using TORO.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class EventMap : BaseMap<Events>, IEntityTypeConfiguration<Events>
    {
        public void Configure(EntityTypeBuilder<Events> builder)
        {
            DefaultConfigs(builder);
            
            builder.Property(x => x.Cpf).IsRequired();
            builder.Property(x => x.IdOrigin).IsRequired();
            builder.Property(x => x.IdTarget).IsRequired();

            builder.Property(x => x.Amount)             
                .IsRequired();

            builder.HasIndex(c => c.Cpf);

            builder.HasOne(x => x.Origin).WithMany()                
                .HasForeignKey(x => x.IdOrigin)
                .OnDelete(DeleteBehavior.Restrict);
            
            builder.HasOne(x => x.Target).WithMany()                
                .HasForeignKey(x => x.IdTarget)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
