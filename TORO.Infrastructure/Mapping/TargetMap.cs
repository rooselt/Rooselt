﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using TORO.Domain.Entities;

namespace TORO.Infra.Data.Mapping
{
    public class TargetMap : BaseMap<Target>, IEntityTypeConfiguration<Target>
    {
        public void Configure(EntityTypeBuilder<Target> builder)
        {
            DefaultConfigs(builder);
                             
            builder.Property(x => x.Account).IsRequired();
            builder.Property(x => x.Bank).IsRequired();
            builder.Property(x => x.Branch).IsRequired();
        }
    }
}
