﻿using TORO.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class BaseMap<T> where T : BaseEntity
    {
        public void DefaultConfigs(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id);  
            builder.Property(x => x.Created).IsRequired(false);
        }
    }
}
