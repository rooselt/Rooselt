﻿
using TORO.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TORO.Infra.Data.Mapping
{
    public class OriginMap : BaseMap<Origin>, IEntityTypeConfiguration<Origin>
    {
        public void Configure(EntityTypeBuilder<Origin> builder)
        {
          
            builder.Property(x => x.Bank).HasMaxLength(20).IsRequired();   
            builder.Property(x => x.Branch).HasMaxLength(20).IsRequired();
            builder.Property(x => x.Cpf).HasMaxLength(11).IsRequired();

        }
    }
}
