﻿
using TORO.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using TORO.Domain.Entities;

namespace TORO.Infra.Data.Mapping
{
    public class TrendsMap : BaseMap<Trends>, IEntityTypeConfiguration<Trends>
    {
        public void Configure(EntityTypeBuilder<Trends> builder)
        {
            DefaultConfigs(builder);

            builder.Property(x => x.Symbol).IsRequired();
            builder.Property(x => x.CurrentPrice).HasColumnType("decimal(18,2)").IsRequired();  

        }
    }
}
