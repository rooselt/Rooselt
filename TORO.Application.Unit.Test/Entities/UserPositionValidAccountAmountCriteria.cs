﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TORO.Domain.Entities;
using Xunit;

namespace TORO.Application.Unit.Test.Services
{
    public class UserPositionValidAccountAmountCriteria
    {
        private List<UserPosition> TestEntries()
        {
            var position1 = new List<Positions>() {
                        new Positions { Amount  = 300 , CurrentPrice = 2, Symbol = "PETR2"  }
                    };
            var positon2 = new List<Positions>() {
                        new Positions { Amount  = 200 , CurrentPrice = 3, Symbol = "PETR2"  }
                    };

            var entries = new List<UserPosition> {
                new UserPosition(position1){
                    Id = 1, IdUser = 1, CheckingAccountAmount = 200

                },
                 new UserPosition(positon2){
                    Id = 2, IdUser = 1, CheckingAccountAmount = 200

                }
            };

            return entries;
        }

        [Fact]
        public void ConsolidatedIsMajorAccountAmount()
        {
            var entries = TestEntries();
            var entriesTo = entries.SingleOrDefault(e => e.Id == 1);
            var amount = entriesTo.Consolidated > entriesTo.CheckingAccountAmount;

            Assert.True(amount);
        }

        [Fact]
        public void AccountAmountIsMajorLimit()
        {
            var entries = TestEntries();
            var entriesTo = entries.SingleOrDefault(e => e.Id == 1);
            var amount = entriesTo.ValidBalanceAmout(100);

            Assert.True(amount);
        }

        [Fact]
        public void AccountAmountIsMinorLimit()
        {
            var entries = TestEntries();
            var entriesTo = entries.SingleOrDefault(e => e.Id == 2);
            var amount = entriesTo.ValidBalanceAmout(300);

            Assert.True(!amount);
        }

    }
}
