﻿using System;
using System.Collections.Generic;
using System.Linq;
using TORO.Application.Specifications;
using TORO.Domain.Entities;
using Xunit;

namespace TORO.Infra.Unit.Test.Specifications
{
    public class TrendsSevenDaysPolicyCriteriaShould
    {
        private List<Order> TestEntries()
        {
            var entries = new List<Order>
            {
                new Order { Id = 1, Created = DateTime.UtcNow, Symbol = "test1", Amount = 1 },
                new Order { Id = 2, Created = DateTime.UtcNow.AddHours(-2), Symbol = "test2", Amount = 2 },
                new Order { Id = 3, Created = DateTime.UtcNow.AddDays(-1), Symbol = "test3", Amount = 3 },
                new Order { Id = 4, Created = DateTime.UtcNow.AddDays(-8), Symbol = "test", Amount = 4 }
            };
            return entries;
        }

        [Fact]
        public void IncludeEntryFromLast7Days()
        {
            var entries = TestEntries();
            var spec = new TrendsSevenDayPolicy();

            var entriesToNotify = entries.Where(spec.Criteria.Compile());

            Assert.NotNull(entriesToNotify.SingleOrDefault(e => e.Id == 2));
            Assert.NotNull(entriesToNotify.SingleOrDefault(e => e.Id == 3));
        }

        [Fact]
        public void NotIncludeEntriesOver7DayOld()
        {
            var entries = TestEntries();
            var spec = new TrendsSevenDayPolicy();

            var entriesToNotify = entries.Where(spec.Criteria.Compile());

            Assert.DoesNotContain(entriesToNotify, e => e.Id == 4);
        }

        [Fact]
        public void IncludeEntriesFromLast24Hours()
        {
            var entries = TestEntries();
            var spec = new TrendsSevenDayPolicy();

            var entriesToNotify = entries.Where(spec.Criteria.Compile()).ToList();

            Assert.NotNull(entriesToNotify.SingleOrDefault(e => e.Id == 2));
            Assert.NotNull(entriesToNotify.SingleOrDefault(e => e.Id == 3));
        }
    }
}
