using System.Collections.Generic;
using System.Text.Json.Serialization;
using TORO.Application.Commom.Enum;

namespace TORO.Domain.Entities
{
    public class User : BaseEntity
    {        
        public string Code { get; set; }
        public string Cpf { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }

        [JsonIgnore]
        public bool IsDeleting { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        [JsonIgnore]
        public List<RefreshToken> RefreshTokens { get; set; }


        public User(string firstName, string lastName, string username, string password)
        {
            this.Code = ((int)BankCode.Code).ToString();
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Username = username;
            this.Password = password;
        }
    }
}