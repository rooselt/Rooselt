﻿using System.Text.Json.Serialization;

namespace TORO.Domain.Entities
{
    public class Positions: BaseEntity
    {

        [JsonIgnore]
        public int IdUserPosition { get; set; }
        public string Symbol { get; set; }
        public int Amount { get; set; }
        public decimal CurrentPrice { get; set; }

        [JsonIgnore]
        public virtual UserPosition UserPosition { get; set; }
    }
}
