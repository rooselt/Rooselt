﻿namespace TORO.Domain.Entities
{
    public class Events : BaseEntity
    {
        public int IdTarget { get; set; }
        public int IdOrigin { get; set; }
        public string Cpf { get; set; }
        public string Event { get; set; }
        public Target Target { get; set; }
        public Origin Origin { get; set; }        
        public int Amount { get; set; }
    }
}
