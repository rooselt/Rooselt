﻿using System.Text.Json.Serialization;
using TORO.Domain.Entities;

namespace TORO.Domain.Entities
{
    public class Origin : BaseEntity
    {
    
        // Banco de origem        
        public string Bank { get; set; }
        // Agencia de origem
        public string Branch { get; set; }
        // CPF do remetente
        public string Cpf { get; set; }

      
    }
}
