﻿using System;
using System.Collections.Generic;

namespace TORO.Domain.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }  
        public DateTime? Created { get; set; } = DateTime.UtcNow;
    }
}
