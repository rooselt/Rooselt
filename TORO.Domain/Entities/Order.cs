﻿using System.Text.Json.Serialization;

namespace TORO.Domain.Entities
{
    public class Order : BaseEntity
    {
        public int IdUserPosition { get; set; }
        public int IdTrend { get; set; }
        public string Symbol { get; set; }
        public int Amount { get; set; }

        [JsonIgnore]
        public virtual UserPosition UserPosition { get; set; }

        [JsonIgnore]
        public virtual Trends Trends { get; set; }
    }
}
