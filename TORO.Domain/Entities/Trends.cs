﻿using System.Collections.Generic;

namespace TORO.Domain.Entities
{
    public class Trends : BaseEntity
    {
        public string Symbol { get; set; }
        public decimal CurrentPrice { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
