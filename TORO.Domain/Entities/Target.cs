﻿
namespace TORO.Domain.Entities
{
    public class Target : BaseEntity
    {       
      
        // Banco Toro
        public string Bank { get; set; }
        
        // Única agenda, sempre 0001
        public string Branch { get; set; }
        
        // Conta do usuário na Toro (unica por usuário)
        public string Account { get; set; }
            
    }
}
