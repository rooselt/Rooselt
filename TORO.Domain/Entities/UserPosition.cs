using System.Collections.Generic;
using System.Linq;

namespace TORO.Domain.Entities
{
    public class UserPosition : BaseEntity
    {
        public int IdUser { get; set; }
        public decimal CheckingAccountAmount { get; set; }

        private decimal consolidated;
        public decimal Consolidated
        {
            get => consolidated = Positions
             .Sum(c => c.CurrentPrice * c.Amount) + CheckingAccountAmount;
            set => consolidated = Positions
             .Sum(c => c.CurrentPrice * c.Amount) + CheckingAccountAmount;
        }
        public virtual User User { get; set; }
        public virtual ICollection<Positions> Positions { get; set; } = new List<Positions>();
        public UserPosition()
        {

        }

        public UserPosition(ICollection<Positions> positions)
        {
            Positions = positions;              
        }     

        public bool ValidBalanceAmout(decimal total)
        {
            return CheckingAccountAmount >= total;
        }
    }
}