﻿
namespace TORO.Application.Commom.Enum
{
    public enum AlertType
    {
        Success,
        Error,
        Info,
        Warning
    }

    public enum BankCode
    {
        Code = 352
    }
}
